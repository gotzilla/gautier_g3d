#ifndef OBJ_HPP
#define OBJ_HPP

#include "./HierarchicalRenderable.hpp"
#include "./lighting/Material.hpp"
#include "./lighting/Light.hpp"
#include "./Io.hpp"
#include "sous_obj.hpp"
#include <list>

#include <string>
#include <vector>
#include <glm/glm.hpp>

class OBJ : public HierarchicalRenderable
{
    public:
        ~OBJ();
        OBJ(
            ShaderProgramPtr program,
            const std::string& mesh_filename);
        void setMaterial(const MaterialPtr& material);

    private:
        void do_draw();
        void do_animate( float time );

        std::vector<sous_objPtr> liste;
      
};

typedef std::shared_ptr<OBJ> OBJPtr;

#endif
