#ifndef SOUS_OBJ_HPP
#define SOUS_OBJ_HPP

#include "./HierarchicalRenderable.hpp"
#include "./lighting/Material.hpp"
#include "./lighting/Light.hpp"

#include <string>
#include <vector>
#include <glm/glm.hpp>

class sous_obj : public HierarchicalRenderable
{
    public:
        ~sous_obj();
        sous_obj(
            ShaderProgramPtr program,
                int index,
                const std::string path);
        void setMaterial(const MaterialPtr& material);
        
        const std::vector<glm::vec3>& getPosition() const;
        const std::vector<glm::vec3>& getNormals() const;
        const std::vector< unsigned int >& getIndice() const;

        void do_draw();
    private:
        void do_animate( float time );

        std::vector< glm::vec3 > m_positions;
        std::vector< glm::vec3 > m_normals;
        std::vector< glm::vec4 > m_colors;
        std::vector< glm::vec2 > m_texCoords;
        std::vector< unsigned int > m_indices;

        unsigned int m_pBuffer;
        unsigned int m_cBuffer;
        unsigned int m_nBuffer;
        unsigned int m_iBuffer;
        unsigned int m_tBuffer;
        unsigned int m_texId;

        bool extense = false;
        std::string texture = "";
        MaterialPtr m_material = Material::Bronze();
};

typedef std::shared_ptr<sous_obj> sous_objPtr;

#endif
