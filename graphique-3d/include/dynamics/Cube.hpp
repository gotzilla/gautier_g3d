#ifndef Cube_HPP
#define Cube_HPP

#include <iostream>
#include <memory>
#include <glm/glm.hpp>
#include <vector>

class Cube {
public:
    Cube(
            const glm::vec3& position,
            const glm::vec3& velocity,
            const glm::vec3& direction,
            const glm::vec3& normale,
            const float& mass,
            const glm::mat4 & model);
    ~Cube();

    const glm::vec3& getPositionPrec() const;
    const glm::vec3& getPosition() const;
    const std::vector<glm::vec3> & getPositions() const;
    const glm::vec3& getDirection() const;
    const glm::vec3& getNormale() const;
    const glm::vec3& getVelocity() const;
    const glm::vec3& getForce() const;
    float getMass() const;
    
    std::vector< glm::vec3 > getForces() const;
    
    void actualise();
    void actualise(glm::vec3 position, glm::vec3 normale, glm::vec3 dir, glm::vec3 vitesse);
    void setPosition(const glm::vec3 &pos);
    void setVelocity(const glm::vec3 &vitesse);
    void setForce(const glm::vec3 &force);
    void addForce(const glm::vec3 &force);
    void incrForce(const glm::vec3 &force);
    void restart();
    const glm::mat4& getModel() const;
    void setModel(const glm::mat4 & model);

    bool au_sol() const;
    void au_sol(bool in);
    bool modif() const;
    void do_animate( float time );
    void turn_right();
    void turn_left();
    void accelerate();
    void decelerate();
    
    
    bool m_left=false;
    bool m_right = false;
    bool m_accelerate = false;
    bool m_decelerate = false;
    float m_last_time = 0;

    
private:
    const glm::vec3 m_initialVelocity;
    const glm::vec3 m_initialPosition;
    const glm::vec3 m_initialNormale;
    const glm::vec3 m_initialDirection;
    std::vector<glm::vec3> m_positions_init;
    std::vector<glm::vec3> m_positions;
    glm::vec3 m_position_prec;
    glm::vec3 m_position;
    glm::vec3 m_direction = glm::vec3(0, 1, 0);
    glm::vec3 m_normale = glm::vec3(0, 0, 1);
    glm::vec3 m_axe = glm::vec3(1, 0, 0);
    glm::vec3 m_velocity;
    glm::vec3 m_force;
    std::vector< glm::vec3 > m_forces;
    float m_mass;
    glm::mat4 m_model;
    bool m_modif=false;
    bool m_au_sol=false;
};

typedef std::shared_ptr<Cube> CubePtr;
#endif


