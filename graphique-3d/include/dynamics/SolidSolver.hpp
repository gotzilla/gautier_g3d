#ifndef SOLIDSOLVER_HPP
#define SOLIDSOLVER_HPP

#include "Solver.hpp"

/**@brief Explicit Euler solver.
 *
 * Explicit Euler dynamic system solver.
 */
class SolidSolver : public Solver
{
public:
    SolidSolver();
    ~SolidSolver();
private:
    void do_solve(const float& dt, std::vector<CubePtr>& kart);
};

typedef std::shared_ptr<SolidSolver> SolidSolverPtr;

#endif //SOLIDSOLVER_HPP
