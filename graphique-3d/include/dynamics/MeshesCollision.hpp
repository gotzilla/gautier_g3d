/* 
 * File:   MeshesCollision.hpp
 * Author: fournieh
 *
 * Created on 12 avril 2016, 12:09
 */

#include "Collision.hpp"
#include "Mesh.hpp"
#include "Cube.hpp"


#ifndef MESHESCOLLISION_HPP
#define	MESHESCOLLISION_HPP

class MeshesCollision : public Collision
{
public:
    ~MeshesCollision();
    MeshesCollision(MeshPtr mesh, CubePtr cube, float restitution, std::vector<int> indice, glm::vec3 I);
private:
    void do_solveCollision();
    
    std::vector<int> m_indice;   
    MeshPtr m_m;
    CubePtr m_c;
    glm::vec3 pt_inter;
};

typedef std::shared_ptr<MeshesCollision> MeshesCollisionPtr;

bool testKartMap(const MeshPtr& m, const CubePtr& c, std::vector<int> &indice, glm::vec3 & out);


#endif	/* MESHESCOLLISION_HPP */

