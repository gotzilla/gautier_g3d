#ifndef Mesh_HPP
#define Mesh_HPP

#include <iostream>
#include <memory>
#include <vector>
#include <glm/glm.hpp>

class Mesh {
public:
    Mesh(const std::vector<glm::vec3>& position,
         const std::vector<glm::vec3>& normals,
         const std::vector< unsigned int > indices,
         const glm::mat4 model);
    ~Mesh();

    const std::vector<int> & getNormals(int x, int y) const;
    const std::vector<glm::vec3>& getPosition() const;
    const std::vector<glm::vec3>& getNormals() const;
    const std::vector<unsigned int>& getIndices(float x, float y) const;
    const std::vector<unsigned int>& getIndices() const;
    const glm::mat4& getModel() const;
    void setModel(const glm::mat4 &model);

private:
    void init();
    std::vector<glm::vec3> m_position;
    std::vector<glm::vec3> m_normals;
    std::vector< unsigned int > m_indices;
    std::vector<unsigned int> ***  m_sup;
    glm::mat4 m_model;
    float x_min;
    float y_min;
    int X;
    int Y;
};

typedef std::shared_ptr<Mesh> MeshPtr;
#endif


