#ifndef SOLVER_HPP
#define SOLVER_HPP

#include <memory>
#include <vector>
#include "Cube.hpp"

class Solver
{
public:
  Solver(){}
  virtual  ~Solver(){}
  void solve( const float& dt, std::vector<CubePtr>& karts );
private:
  virtual void do_solve(const float& dt, std::vector<CubePtr>& karts) = 0;
};

typedef std::shared_ptr<Solver> SolverPtr;

#endif //SOLVER_HPP
