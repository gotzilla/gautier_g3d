#ifndef CONSTANT_FORCE_FIELD_HPP
#define CONSTANT_FORCE_FIELD_HPP

#include <vector>
#include "ForceField.hpp"
#include "Particle.hpp"
#include "Cube.hpp"
#include "Mesh.hpp"

/**@brief Constant force field.
 *
 * Implementation of a force field that is constant, i.e. the same for
 * all managed cubes.
 */
class ConstantForceField : public ForceField
{
    public:
        /**@brief Build a constant force field.
         *
         * Build a constant force field for a set of cubes.
         * @param cubes The set of cubes influenced by this force field.
         * @param force The constant force applied to all cubes.
         */
        ConstantForceField(const std::vector<CubePtr>& cubes, const glm::vec3& force);

        /**@brief Access to the set of managed cubes.
         *
         * Get the set of managed cubes of this constant force field.
         * @return The managed force field.
         */
        const std::vector<CubePtr> getKarts();

        /**@brief Define a new set of cubes managed by this constant force field.
         *
         * Set the cubes influenced by this constant force field.
         * @param cubes The new set of cubes.
         */
        void setKarts(const std::vector<CubePtr>& cubes);

        /**@brief Access to the force applied to all influenced cubes.
         *
         * Get the constant force of this force field.
         * @return The force of this force field.
         */
        const glm::vec3& getForce();

        /**@brief Set the force applied to all influenced cubes.
         *
         * Set the force applied to all cubes influenced by this force field.
         * @param force The new force.
         */
        void setForce(const glm::vec3& force);

    private:
        void do_addForce();
        std::vector<CubePtr> m_karts;
        glm::vec3 m_force;
};

typedef std::shared_ptr<ConstantForceField> ConstantForceFieldPtr;

#endif // SPRING_HPP

