#ifndef CUBE_RENDERABLE_HPP
#define CUBE_RENDERABLE_HPP

#include "./../include/HierarchicalRenderable.hpp"
#include "Renderable.hpp"
#include <vector>
#include "./../include/dynamics/Cube.hpp"
#include <glm/glm.hpp>

class CubeRenderable : public HierarchicalRenderable
{
    public:
        ~CubeRenderable();
        CubeRenderable();
        CubeRenderable( ShaderProgramPtr program, CubePtr kart );
        glm::vec3 approximatedCenter;
        std::vector< glm::vec3 >& getPositions();
        float zMin;
        float zMax;
        float xMax;
        float xMin;
        float yMin;
        float yMax;
    private:
        void do_draw();
        void do_keyPressedEvent( sf::Event& e );
        void do_keyReleasedEvent( sf::Event& e );
        void do_animate( float time );
        
        CubePtr m_cube;
        std::vector< glm::vec3 > m_positions;
        std::vector< glm::vec4 > m_colors;
        std::vector< glm::vec3 > m_normals;
        

        unsigned int m_pBuffer;
        unsigned int m_cBuffer;
        unsigned int m_nBuffer;
};

typedef std::shared_ptr<CubeRenderable> CubeRenderablePtr;

#endif
