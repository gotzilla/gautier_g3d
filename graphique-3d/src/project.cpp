#include "./../include/project.hpp"

#include "../include/ShaderProgram.hpp"

#include "../include/lighting/LightedMeshRenderable.hpp"
#include "../include/CubeRenderable.hpp"
#include "../include/lighting/LightedCylinderRenderable.hpp"
#include "../include/lighting/DirectionalLightRenderable.hpp"
#include "../include/lighting/PointLightRenderable.hpp"
#include "../include/lighting/SpotLightRenderable.hpp"
#include "../include/FrameRenderable.hpp"
#include "../include/dynamics/DynamicSystem.hpp"
#include "../include/dynamics/DynamicSystemRenderable.hpp"
#include "../include/log.hpp"
#include "../include/dynamics/DampingForceField.hpp"
#include "../include/dynamics/ConstantForceField.hpp"
#include "../include/dynamics/SpringForceField.hpp"
#include "../include/dynamics/SolidSolver.hpp"
#include "../include/dynamics/MeshesCollision.hpp"
#include "../include/OBJ.hpp"
#include "../include/dynamics/ConstantForceFieldRenderable.hpp"
#include "../include/dynamics/SpringForceFieldRenderable.hpp"
#include "../include/dynamics/ControlledForceFieldRenderable.hpp"

#include <iostream>

void initialize_project(Viewer& viewer) {

//    //Default shader
    ShaderProgramPtr flatShader = std::make_shared<ShaderProgram>("../shaders/flatVertex.glsl", "../shaders/flatFragment.glsl");
    viewer.addShaderProgram(flatShader);
//
//    //Define a shader that encode an illumination model
    ShaderProgramPtr phongShader = std::make_shared<ShaderProgram>("../shaders/phongVertex.glsl", "../shaders/phongFragment.glsl");
    viewer.addShaderProgram(phongShader);
//
//    //Add a 3D frame to the viewer
//    FrameRenderablePtr frame = std::make_shared<FrameRenderable>(flatShader);
//    viewer.addRenderable(frame);
//
//    //Initialize a dynamic system (Solver, Time step, Restitution coefficient)
    DynamicSystemPtr system = std::make_shared<DynamicSystem>();
    SolidSolverPtr solver = std::make_shared<SolidSolver>();
    system->setSolver(solver);
    system->setDt(0.01);
//    
//    
//    //Create a renderable associated to the dynamic system
//    //This renderable is responsible for calling DynamicSystem::computeSimulationStep() in the animate() function
//    //It is also responsible for some of the key/mouse events
    DynamicSystemRenderablePtr systemRenderable = std::make_shared<DynamicSystemRenderable>(system);
    viewer.addRenderable(systemRenderable);
//
//    //Define a transformation
   glm::mat4 parentTransformation, localTransformation;
//
//    //Define a directional light for the whole scene
//    glm::vec3 d_direction = glm::normalize(glm::vec3(0.0, -1.0, -1.0));
//    glm::vec3 d_ambient(0.0, 0.0, 0.0), d_diffuse(0.3, 0.3, 0.1), d_specular(0.3, 0.3, 0.1);
//    //glm::vec3 d_ambient(0.0,0.0,0.0), d_diffuse(0.0,0.0,0.0), d_specular(0.0,0.0,0.0);
//    DirectionalLightPtr directionalLight = std::make_shared<DirectionalLight>(d_direction, d_ambient, d_diffuse, d_specular);
//    //Add a renderable to display the light and control it via mouse/key event
//    glm::vec3 lightPosition(0.0, 5.0, 8.0);
//    DirectionalLightRenderablePtr directionalLightRenderable = std::make_shared<DirectionalLightRenderable>(flatShader, directionalLight, lightPosition);
//    localTransformation = glm::scale(glm::mat4(1.0), glm::vec3(0.5, 0.5, 0.5));
//    directionalLightRenderable->setLocalTransform(localTransformation);
//    viewer.setDirectionalLight(directionalLight);
//    viewer.addRenderable(directionalLightRenderable);
//
//    //Define a point light
//    glm::vec3 p_position(0.0, 0.0, 0.0), p_ambient(0.0, 0.0, 0.0), p_diffuse(0.0, 0.0, 0.0), p_specular(0.0, 0.0, 0.0);
//    float p_constant = 0.0, p_linear = 0.0, p_quadratic = 0.0;
//
//    p_position = glm::vec3(-8, 5.0, 5.0);
//    p_ambient = glm::vec3(0.0, 0.0, 0.0);
//    p_diffuse = glm::vec3(1.0, 0.0, 0.0);
//    p_specular = glm::vec3(1.0, 0.0, 0.0);
//    p_constant = 1.0;
//    p_linear = 5e-1;
//    p_quadratic = 0;
//    PointLightPtr pointLight1 = std::make_shared<PointLight>(p_position, p_ambient, p_diffuse, p_specular, p_constant, p_linear, p_quadratic);
//    PointLightRenderablePtr pointLightRenderable1 = std::make_shared<PointLightRenderable>(flatShader, pointLight1);
//    localTransformation = glm::scale(glm::mat4(1.0), glm::vec3(0.5, 0.5, 0.5));
//    pointLightRenderable1->setLocalTransform(localTransformation);
//    viewer.addPointLight(pointLight1);
//    viewer.addRenderable(pointLightRenderable1);
//
//    p_position = glm::vec3(8, 5.0, 5.0);
//    p_ambient = glm::vec3(0.0, 0.0, 0.0);
//    p_diffuse = glm::vec3(0.0, 0.0, 1.0);
//    p_specular = glm::vec3(0.0, 0.0, 1.0);
//    p_constant = 1.0;
//    p_linear = 5e-1;
//    p_quadratic = 0;
//    PointLightPtr pointLight2 = std::make_shared<PointLight>(p_position, p_ambient, p_diffuse, p_specular, p_constant, p_linear, p_quadratic);
//    PointLightRenderablePtr pointLightRenderable2 = std::make_shared<PointLightRenderable>(flatShader, pointLight2);
//    localTransformation = glm::scale(glm::mat4(1.0), glm::vec3(0.5, 0.5, 0.5));
//    pointLightRenderable2->setLocalTransform(localTransformation);
//    viewer.addPointLight(pointLight2);
//    viewer.addRenderable(pointLightRenderable2);
//
////    //Define a spot light
//    glm::vec3 s_position(0.0, 5.0, -8.0), s_spotDirection = glm::normalize(glm::vec3(0.0, -1.0, 1.0));
//    //glm::vec3 s_ambient(0.0,0.0,0.0), s_diffuse(0.0,0.0,0.0), s_specular(0.0,0.0,0.0);
//    glm::vec3 s_ambient(0.0, 0.0, 0.0), s_diffuse(0.5, 0.5, 0.5), s_specular(0.5, 0.5, 0.5);
//    float s_constant = 1.0, s_linear = 0.0, s_quadratic = 0.0;
//    float s_innerCutOff = std::cos(glm::radians(20.0f)), s_outerCutOff = std::cos(glm::radians(40.0f));
//    SpotLightPtr spotLight = std::make_shared<SpotLight>(s_position, s_spotDirection,
//            s_ambient, s_diffuse, s_specular,
//            s_constant, s_linear, s_quadratic,
//            s_innerCutOff, s_outerCutOff);
//    SpotLightRenderablePtr spotLightRenderable = std::make_shared<SpotLightRenderable>(flatShader, spotLight);
//    localTransformation = glm::scale(glm::mat4(1.0), glm::vec3(0.5, 0.5, 0.5));
//    spotLightRenderable->setLocalTransform(localTransformation);
//    viewer.addSpotLight(spotLight);
//    viewer.addRenderable(spotLightRenderable);
//    
//    
//    
//    
//  //  charger la lumiere
    parentTransformation = localTransformation = glm::mat4(1.0);
    glm::vec3 d_direction = glm::normalize(glm::vec3(0.0,-1.0,0));
    glm::vec3 d_ambient(1.0,1.0,1.0), d_diffuse(1.0,1.0,0.8), d_specular(1.0,1.0,1.0);
    DirectionalLightPtr directionalLight = std::make_shared<DirectionalLight>(d_direction, d_ambient, d_diffuse, d_specular);
    //Add a renderable to display the light and control it via mouse/key event
    glm::vec3 lightPosition(0.0,5000.0,0);
    DirectionalLightRenderablePtr directionalLightRenderable = std::make_shared<DirectionalLightRenderable>(flatShader, directionalLight, lightPosition);
    localTransformation = glm::scale(glm::mat4(1.0), glm::vec3(0.5,0.5,0.5));
    directionalLightRenderable->setLocalTransform(localTransformation);
    viewer.setDirectionalLight(directionalLight);
    viewer.addRenderable(directionalLightRenderable);

    MaterialPtr plastic = std::make_shared<Material>(glm::vec3(0, 0, 0), glm::vec3(0.55, 0.55, 0.55), glm::vec3(0.70, 0.70, 0.70), 0.25 * 128);
    ShaderProgramPtr nullShader = std::make_shared<ShaderProgram>("../shaders/phongVertex.glsl", "../shaders/phongFragment.glsl");
    plastic = Material::Pearl();
    
    
    
   // LightedMeshRenderablePtr map = std::make_shared<LightedMeshRenderable>(nullShader, "./../meshes/obj/neige2.obj");
    
    
    LightedMeshRenderablePtr map = std::make_shared<LightedMeshRenderable>(phongShader, "./../meshes/obj/neige2.obj");
    HierarchicalRenderable::addChild(systemRenderable, map);

    
    
    
    
    
//    localTransformation = glm::mat4(1.0);
//    parentTransformation = glm::translate(glm::mat4(1.0f), glm::vec3(0,0,-10));
////    parentTransformation = glm::scale(glm::mat4(1.0f), glm::vec3(10.0f));
//    parentTransformation = glm::rotate(parentTransformation, (float) M_PI /2 , glm::vec3(1,0,0));
////    map->setParentTransform(parentTransformation);
////    map->setLocalTransform(localTransformation);
//    glm::mat4 transf = map->getParentTransform();
//    transf = glm::transpose(transf);
//    printf("%f, %f, %f, %f\n",transf[0][0],transf[1][0],transf[2][0],transf[3][0]);
//    printf("%f, %f, %f, %f\n",transf[0][1],transf[1][1],transf[2][1],transf[3][1]);
//    printf("%f, %f, %f, %f\n",transf[0][2],transf[1][2],transf[2][2],transf[3][2]);
//    printf("%f, %f, %f, %f\n",transf[0][3],transf[1][3],transf[2][3],transf[3][3]);
    map->setMaterial(plastic);
    ShaderProgramPtr texShader = std::make_shared<ShaderProgram>("../shaders/textureVertex.glsl","../shaders/textureFragment.glsl");
    viewer.addShaderProgram( texShader );
    OBJPtr map2 = std::make_shared<OBJ>(texShader, "../meshes/obj/neige.obj");
    viewer.addRenderable(map2);  
    MeshPtr mesh = std::make_shared<Mesh>(map->getPositions(), map->getNormals(), map->getIndices(), map->getModelMatrix());
    system->setMap(mesh);

    //mesh->getParentTransform();
    //    LightedMeshRenderablePtr kart = std::make_shared<LightedMeshRenderable>(phongShader, "./../meshes/Kart.obj");
//    HierarchicalRenderable::addChild(systemRenderable, kart);
//    //    parentTransformation = glm::rotate(glm::mat4(1.0), (float) 90, glm::vec3(1.0, 0.0, 0.0));
//    //    glm::translate(parentTransformation, glm::vec3(0.0, 0.0, 5.0));
//    localTransformation = glm::mat4(1.0);
//    parentTransformation = glm::scale(glm::mat4(1.0f), glm::vec3(10.f));
//    kart->setParentTransform(parentTransformation);
//    kart->setLocalTransform(localTransformation);
//    kart->setMaterial(plastic);
//    viewer.addRenderable(kart);

    
    
    
    
    
    
    
    d_direction = glm::normalize(glm::vec3(0.0,0.0,-1.0));
    d_ambient = glm::vec3(1.0,1.0,1.0);
    d_diffuse = glm::vec3(1.0,1.0,0.8);
    d_specular= glm::vec3(1.0,1.0,1.0);
    directionalLight = std::make_shared<DirectionalLight>(d_direction, d_ambient, d_diffuse, d_specular);
    //Add a renderable to display the light and control it via mouse/key event
    lightPosition=glm::vec3(0.0,5000.0,0);
    directionalLightRenderable = std::make_shared<DirectionalLightRenderable>(flatShader, directionalLight, lightPosition);
    localTransformation = glm::scale(glm::mat4(1.0), glm::vec3(0.5,0.5,0.5));
    directionalLightRenderable->setLocalTransform(localTransformation);
    viewer.setDirectionalLight(directionalLight);
    viewer.addRenderable(directionalLightRenderable);
    
    
    
    
    
    
    
    
    
    //Kart HitBox
    
    CubePtr cube = std::make_shared<Cube>(glm::vec3(0,0,0), glm::vec3(0,0,0), glm::vec3(1,0,0), glm::vec3(0,0,1), 10, glm::mat4(1.0));
    CubeRenderablePtr kartHB = std::make_shared<CubeRenderable>(flatShader, cube);
    viewer.set_cam(cube);
    system->addKart(cube);
    HierarchicalRenderable::addChild(systemRenderable, kartHB);
    //parentTransformation = glm::rotate(glm::mat4(1.0), (float) 90, glm::vec3(1.0, 0.0, 0.0));
    localTransformation = glm::mat4(1.0);
    parentTransformation = localTransformation; // glm::translate(glm::mat4(1.0), kart->approximatedCenter);
    //glm::scale(parentTransformation, glm::vec3(0.8f));
    kartHB->setParentTransform(parentTransformation);
    kartHB->setLocalTransform(localTransformation);
    viewer.addRenderable(kartHB);
    //HierarchicalRenderable::addChild(kart, kartHB);
    
     //Initialize a force field that apply to all the particles of the system to simulate gravity
    //Add it to the system as a force field
    
    ConstantForceFieldPtr gravityForceField = std::make_shared<ConstantForceField>(system->getKarts(), glm::vec3{0,0,-10} );
    system->addForceField( gravityForceField );
    
    //Activate collision and set the restitution coefficient to 1.0
    system->setCollisionsDetection(true);
    system->setRestitution(1.0f);
    
    //Position the camera
    glm::vec3 cameraPos = kartHB->approximatedCenter;
    cameraPos.z += 1;
    cameraPos.x -= 1;
    cameraPos.y -= 0;
    viewer.getCamera().setViewMatrix(glm::lookAt(cameraPos, kartHB->approximatedCenter, glm::vec3(0, 0, 1)));
    viewer.startAnimation();

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
//    
//  //SAUVEGARDE  
//    //charger le shader
//    ShaderProgramPtr flatShader = std::make_shared<ShaderProgram>("../shaders/flatVertex.glsl","../shaders/flatFragment.glsl");
//    viewer.addShaderProgram( flatShader );
// 
//    //charge les frames
//    FrameRenderablePtr frame = std::make_shared<FrameRenderable>(flatShader);
//    viewer.addRenderable(frame);
//    
//    
//    //charger la lumiere
//    glm::mat4 parentTransformation(1.0), localTransformation(1.0);
//    glm::vec3 d_direction = glm::normalize(glm::vec3(0.0,-1.0,0));
//    glm::vec3 d_ambient(1.0,1.0,1.0), d_diffuse(1.0,1.0,0.8), d_specular(1.0,1.0,1.0);
//    DirectionalLightPtr directionalLight = std::make_shared<DirectionalLight>(d_direction, d_ambient, d_diffuse, d_specular);
//    //Add a renderable to display the light and control it via mouse/key event
//    glm::vec3 lightPosition(0.0,5000.0,0);
//    DirectionalLightRenderablePtr directionalLightRenderable = std::make_shared<DirectionalLightRenderable>(flatShader, directionalLight, lightPosition);
//    localTransformation = glm::scale(glm::mat4(1.0), glm::vec3(0.5,0.5,0.5));
//    directionalLightRenderable->setLocalTransform(localTransformation);
//    viewer.setDirectionalLight(directionalLight);
//    viewer.addRenderable(directionalLightRenderable);
//
//    //charger la map choisie
//    //Textured shader
//    ShaderProgramPtr texShader = std::make_shared<ShaderProgram>("../shaders/textureVertex.glsl","../shaders/textureFragment.glsl");
//    viewer.addShaderProgram( texShader );
//    OBJPtr circuit = std::make_shared<OBJ>(texShader, "../meshes/cube.obj");
//    viewer.addRenderable(circuit);
//    //la transformer
//    
//    //charger le(s) kart(s)
//    
//    //les transformer
//    
//    //placer le(s) camera(s)
//    
//

}

