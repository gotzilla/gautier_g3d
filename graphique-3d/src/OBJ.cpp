#include "./../include/OBJ.hpp"
#include "./../include/gl_helper.hpp"
#include "./../include/log.hpp"
#include "./../include/Io.hpp"
#include "./../include/Utils.hpp"
#include <list>

#include <glm/gtc/type_ptr.hpp>
#include <GL/glew.h>

OBJ::~OBJ()
{
    //suppression de la liste
    liste.clear();
}

OBJ::OBJ(
    ShaderProgramPtr shaderProgram, const std::string& mesh_filename) :
    HierarchicalRenderable(shaderProgram)
{
    int nb_sous_objet;
    std::string path = "";
    open_obj(mesh_filename, nb_sous_objet, path);
    for(int i =0; i<nb_sous_objet; i++)
    {
        sous_objPtr nouv = std::make_shared<sous_obj>(shaderProgram, i, path);
        liste.push_back(nouv);
    }
//creation de la liste


    free_obj();
    
}
void OBJ::do_draw() {

    for (int so =0; so<liste.size(); so++)
    {
        liste[so]->do_draw();
    }   
}
void OBJ::do_animate(float time) {}

void OBJ::setMaterial(const MaterialPtr& material)
{
    for (int so =0; so<liste.size(); so++)
    {
        liste[so]->setMaterial(material);
    }   
}

