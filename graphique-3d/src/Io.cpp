#include "./../include/Io.hpp"
#include "./../include/log.hpp"
#include <iostream>
#include "./../include/lighting/Material.hpp"

#define TINYOBJLOADER_IMPLEMENTATION // define this in only *one* .cc
#include "tiny_obj_loader.h"



    std::vector<tinyobj::shape_t> shapes;
    std::vector<tinyobj::material_t> materials;



bool read_obj(const std::string& filename,
        std::vector<glm::vec3>& positions,
        std::vector<unsigned int>& triangles,
        std::vector<glm::vec3>& normals,
        std::vector<glm::vec2>& texcoords,
        glm::vec3 *approximateCenter
        ) {
    std::string err;
    shapes.clear();
    materials.clear();
    bool ret = tinyobj::LoadObj(shapes, materials, err, filename.c_str());

    if (!err.empty()) {
        std::cerr << err << std::endl;
    }

    if (!ret) {
        LOG(debug, "failed to load OBJ, return code " << ret);
        return ret;
    }

    positions.clear();
    triangles.clear();
    normals.clear();
    texcoords.clear();

    float minx = shapes[0].mesh.positions[0];
    float miny = shapes[0].mesh.positions[1];
    float minz = shapes[0].mesh.positions[2];
    float maxx = shapes[0].mesh.positions[0];
    float maxy = shapes[0].mesh.positions[1];
    float maxz = shapes[0].mesh.positions[2];

    for (size_t i = 0; i < shapes.size(); i++) {
        assert((shapes[i].mesh.indices.size() % 3) == 0);
        for (size_t f = 0; f < shapes[i].mesh.indices.size(); f++) {
            triangles.push_back(shapes[i].mesh.indices[f]);
        }
        assert((shapes[i].mesh.positions.size() % 3) == 0);
        for (size_t v = 0; v < shapes[i].mesh.positions.size() / 3; v++) {
            
            float x = shapes[i].mesh.positions[3 * v + 0];
            float y = shapes[i].mesh.positions[3 * v + 1];
            float z = shapes[i].mesh.positions[3 * v + 2];
            
            if (x > maxx) {
                maxx = (float) x;
            }
            if (y > maxy) {
                maxy = (float) y;
            }
            if (z > maxz) {
                maxz = (float) z;
            }
            
            if (x < minx) {
                minx = (float) x;
            }
            if (y < miny) {
                miny = (float) y;
            }
            if (z < minz) {
                minz = (float) z;
            }
            *approximateCenter = glm::vec3((maxx+minx)/2, (maxy+miny)/2, (maxz+minz)/2);
            positions.push_back(glm::vec3(shapes[i].mesh.positions[3 * v + 0], shapes[i].mesh.positions[3 * v + 1], shapes[i].mesh.positions[3 * v + 2]));
        }

        assert((shapes[i].mesh.normals.size() % 3) == 0);
        for (size_t n = 0; n < shapes[i].mesh.normals.size() / 3; n++) {
            normals.push_back(glm::vec3(shapes[i].mesh.normals[3 * n + 0], shapes[i].mesh.normals[3 * n + 1], shapes[i].mesh.normals[3 * n + 2]));
        }

        assert((shapes[i].mesh.texcoords.size() % 2) == 0);
        for (size_t t = 0; t < shapes[i].mesh.texcoords.size() / 2; t++) {
            texcoords.push_back(glm::vec2(shapes[i].mesh.texcoords[2 * t + 0], shapes[i].mesh.texcoords[2 * t + 1]));
        }
    }

    return ret;
}

bool open_obj(const std::string& filename,
        int &nb_shapes,
        std::string &path
        ) {
    shapes.clear();
    materials.clear();
    std::string err;
       
    
    int i = 0;
    int cmpt = filename.size();
    while(filename[i]!= '\n'&& i<filename.size()) {
        if(filename[i]=='/') {
            cmpt = i+1;
        }
        i++;
    }
    path = filename.substr(0,cmpt).c_str();
    bool ret = tinyobj::LoadObj(shapes, materials, err, filename.c_str(), path.c_str());

    if (!err.empty()) {
        std::cerr << err << std::endl;
    }

    if (!ret) {
        LOG(debug, "failed to load OBJ, return code " << ret);
        return ret;
    }
    nb_shapes = shapes.size();
    return ret;
}

void create_obj(std::vector<glm::vec3>& positions, 
        std::vector<unsigned int>& triangles,
        std::vector<glm::vec3>& normals,
        std::vector<glm::vec2>& texcoords,
        MaterialPtr &material,
        std::string &text_name,
        int i,
        const std::string path)
{

    
    positions.clear();
    triangles.clear();
    normals.clear();
    texcoords.clear();
        assert((shapes[i].mesh.indices.size() % 3) == 0);
        for (size_t f = 0; f < shapes[i].mesh.indices.size(); f++) {
            triangles.push_back(shapes[i].mesh.indices[f]);
        }
        assert((shapes[i].mesh.positions.size() % 3) == 0);
        for (size_t v = 0; v < shapes[i].mesh.positions.size() / 3; v++) {
            positions.push_back(glm::vec3(shapes[i].mesh.positions[3 * v + 0], shapes[i].mesh.positions[3 * v + 1], shapes[i].mesh.positions[3 * v + 2]));
        }

        assert((shapes[i].mesh.normals.size() % 3) == 0);
        for (size_t n = 0; n < shapes[i].mesh.normals.size() / 3; n++) {
            normals.push_back(glm::vec3(shapes[i].mesh.normals[3 * n + 0], shapes[i].mesh.normals[3 * n + 1], shapes[i].mesh.normals[3 * n + 2]));
        }

        assert((shapes[i].mesh.texcoords.size() % 2) == 0);
        for (size_t t = 0; t < shapes[i].mesh.texcoords.size() / 2; t++) {
            texcoords.push_back(glm::vec2(shapes[i].mesh.texcoords[2 * t + 0], shapes[i].mesh.texcoords[2 * t + 1]));
        }

        int id_m = shapes[i].mesh.material_ids[0];
        text_name = "";
        if(id_m<materials.size()&&id_m>=0){
            //creation materiaux et textures
            glm::vec3 ambient;
            glm::vec3 diffuse;
            glm::vec3 specular;
        
            for(int j = 0; j<3; j++){
                ambient[j] = materials[id_m].ambient[j];
                diffuse[j] = materials[id_m].diffuse[j];
                specular[j] = materials[id_m].specular[j];
            }
        
            material = std::make_shared<Material>(ambient, diffuse, specular, materials[id_m].shininess);
            if(!materials[id_m].diffuse_texname.empty()) {
                text_name += path.c_str();
                //printf("\n%s\n", text_name.c_str());
                text_name += materials[id_m].diffuse_texname.c_str();
                //printf("%s\n\n", text_name.c_str());
        
            }
        }else {
            material = Material::Pearl();
    
        }
}

void free_obj() {
    shapes.clear();
    materials.clear();
    
}
bool read_obj(const std::string& filename,
        std::vector<glm::vec3>& positions,
        std::vector<unsigned int>& triangles,
        std::vector<glm::vec3>& normals,
        std::vector<glm::vec2>& texcoords)
{
    shapes.clear();
    materials.clear();
    positions.clear();
    triangles.clear();
    normals.clear();
    texcoords.clear();
    std::string err;
    const std::string path = filename;
    
    
    int i = 0;
    int cmpt = filename.size();
    while(path[i]!= '\n') {
        if(path[i]=='/') {
            cmpt = i+1;
        }
        i++;
    }
    
    bool ret = tinyobj::LoadObj(shapes, materials, err, filename.c_str(), filename.substr(0,cmpt).c_str());

    if (!err.empty()) {
        std::cerr << err << std::endl;
    }

    if (!ret) {
        LOG(debug, "failed to load OBJ, return code " << ret);
        return ret;
    }
    for(i=0; i<shapes.size(); i++) {
        assert((shapes[i].mesh.indices.size() % 3) == 0);
        for (size_t f = 0; f < shapes[i].mesh.indices.size(); f++) {
            triangles.push_back(shapes[i].mesh.indices[f]);
        }
        assert((shapes[i].mesh.positions.size() % 3) == 0);
        for (size_t v = 0; v < shapes[i].mesh.positions.size() / 3; v++) {
            positions.push_back(glm::vec3(shapes[i].mesh.positions[3 * v + 0], shapes[i].mesh.positions[3 * v + 1], shapes[i].mesh.positions[3 * v + 2]));
        }

        assert((shapes[i].mesh.normals.size() % 3) == 0);
        for (size_t n = 0; n < shapes[i].mesh.normals.size() / 3; n++) {
            normals.push_back(glm::vec3(shapes[i].mesh.normals[3 * n + 0], shapes[i].mesh.normals[3 * n + 1], shapes[i].mesh.normals[3 * n + 2]));
        }

        assert((shapes[i].mesh.texcoords.size() % 2) == 0);
        for (size_t t = 0; t < shapes[i].mesh.texcoords.size() / 2; t++) {
            texcoords.push_back(glm::vec2(shapes[i].mesh.texcoords[2 * t + 0], shapes[i].mesh.texcoords[2 * t + 1]));
        }

    }
    shapes.clear();
    materials.clear();
    return ret;
    

}

    void debuger() {
//    
//    std::cout << "# of shapes    : " << shapes.size() << std::endl;
//    std::cout << "# of materials : " << materials.size() << std::endl;
//
//    for (size_t i = 0; i < shapes.size(); i++) {
//      printf("shape[%ld].name = %s\n", i, shapes[i].name.c_str());
//      printf("Size of shape[%ld].indices: %ld\n", i, shapes[i].mesh.indices.size());
//      printf("Size of shape[%ld].material_ids: %ld\n", i, shapes[i].mesh.material_ids.size());
//      assert((shapes[i].mesh.indices.size() % 3) == 0);
//      for (size_t f = 0; f < shapes[i].mesh.indices.size() / 3; f++) {
//        printf("  idx[%ld] = %d, %d, %d. mat_id = %d\n", f, shapes[i].mesh.indices[3*f+0], shapes[i].mesh.indices[3*f+1], shapes[i].mesh.indices[3*f+2], shapes[i].mesh.material_ids[f]);
//      }
//
//      printf("shape[%ld].vertices: %ld\n", i, shapes[i].mesh.positions.size());
//      assert((shapes[i].mesh.positions.size() % 3) == 0);
//      for (size_t v = 0; v < shapes[i].mesh.positions.size() / 3; v++) {
//        printf("  v[%ld] = (%f, %f, %f)\n", v,
//          shapes[i].mesh.positions[3*v+0],
//          shapes[i].mesh.positions[3*v+1],
//          shapes[i].mesh.positions[3*v+2]);
//      }
//    }

    for (size_t i = 0; i < materials.size(); i++) {
      printf("material[%ld].name = %s\n", i, materials[i].name.c_str());
      printf("  material.Ka = (%f, %f ,%f)\n", materials[i].ambient[0], materials[i].ambient[1], materials[i].ambient[2]);
      printf("  material.Kd = (%f, %f ,%f)\n", materials[i].diffuse[0], materials[i].diffuse[1], materials[i].diffuse[2]);
      printf("  material.Ks = (%f, %f ,%f)\n", materials[i].specular[0], materials[i].specular[1], materials[i].specular[2]);
      printf("  material.Tr = (%f, %f ,%f)\n", materials[i].transmittance[0], materials[i].transmittance[1], materials[i].transmittance[2]);
      printf("  material.Ke = (%f, %f ,%f)\n", materials[i].emission[0], materials[i].emission[1], materials[i].emission[2]);
      printf("  material.Ns = %f\n", materials[i].shininess);
      printf("  material.Ni = %f\n", materials[i].ior);
      printf("  material.dissolve = %f\n", materials[i].dissolve);
      printf("  material.illum = %d\n", materials[i].illum);
      printf("  material.map_Ka = %s\n", materials[i].ambient_texname.c_str());
      printf("  material.map_Kd = %s\n", materials[i].diffuse_texname.c_str());
      printf("  material.map_Ks = %s\n", materials[i].specular_texname.c_str());
      printf("  material.map_Ns = %s\n", materials[i].specular_highlight_texname.c_str());
      std::map<std::string, std::string>::const_iterator it(materials[i].unknown_parameter.begin());
      std::map<std::string, std::string>::const_iterator itEnd(materials[i].unknown_parameter.end());
      for (; it != itEnd; it++) {
        printf("  material.%s = %s\n", it->first.c_str(), it->second.c_str());
      }
      printf("\n");
    }


     

    }
