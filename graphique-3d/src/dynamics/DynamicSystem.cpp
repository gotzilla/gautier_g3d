#include <cmath>
#include <iostream>
#include <GL/glew.h>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/norm.hpp>

#include "./../../include/gl_helper.hpp"
#include "./../../include/dynamics/DynamicSystem.hpp"
#include "./../../include/dynamics/ParticlePlaneCollision.hpp"
#include "./../../include/dynamics/ParticleParticleCollision.hpp"
#include "./../../include/dynamics/MeshesCollision.hpp"
#include "../../include/log.hpp"
DynamicSystem::DynamicSystem() :
m_dt(0.1),
m_restitution(1.0),
m_handleCollisions(true) {
}

void DynamicSystem::addKart(CubePtr &kart){
    m_karts.push_back(kart);
}

const std::vector<ParticlePtr>& DynamicSystem::getParticles() const {
    return m_particles;
}

const std::vector<CubePtr>& DynamicSystem::getKarts() const {
    return m_karts;
}

const MeshPtr& DynamicSystem::getMap() const {
    return m_map;
}

void DynamicSystem::setParticles(const std::vector<ParticlePtr> &particles) {
    m_particles = particles;
}

void DynamicSystem::setKarts(const std::vector<CubePtr> &karts) {
    m_karts = karts;
}

void DynamicSystem::setMap(const MeshPtr &map) {
    m_map = map;
}

const std::vector<ForceFieldPtr>& DynamicSystem::getForceFields() const {
    return m_forceFields;
}

void DynamicSystem::setForceFields(const std::vector<ForceFieldPtr> &forceFields) {
    m_forceFields = forceFields;
}

float DynamicSystem::getDt() const {
    return m_dt;
}

void DynamicSystem::setDt(float dt) {
    m_dt = dt;
}

DynamicSystem::~DynamicSystem() {
}

void DynamicSystem::clear() {
    m_particles.clear();
    m_forceFields.clear();
    m_planeObstacles.clear();
}

bool DynamicSystem::getCollisionDetection() {
    return m_handleCollisions;
}

void DynamicSystem::setCollisionsDetection(bool onOff) {
    m_handleCollisions = onOff;
}

void DynamicSystem::addParticle(ParticlePtr p) {
    m_particles.push_back(p);
}

void DynamicSystem::addForceField(ForceFieldPtr forceField) {
    m_forceFields.push_back(forceField);
}

void DynamicSystem::addPlaneObstacle(PlanePtr planeObstacle) {
    m_planeObstacles.push_back(planeObstacle);
}

SolverPtr DynamicSystem::getSolver() {
    return m_solver;
}

void DynamicSystem::setSolver(SolverPtr solver) {
    m_solver = solver;
}

void DynamicSystem::detectCollisions() {
    //Detect kart map collisions
    std::vector<int> indice;
    for (CubePtr k : m_karts) {
        indice.clear();
        glm::vec3 I;
        if (testKartMap(getMap(),k, indice, I)) {
            MeshesCollisionPtr c = std::make_shared<MeshesCollision>(getMap(), k, m_restitution, indice, I);
            m_collisions.push_back(c);
        }
    }

    //Detect particle particle collisions
//    for (size_t i = 0; i < m_particles.size(); ++i) {
//        for (size_t j = i; j < m_particles.size(); ++j) {
//            ParticlePtr p1 = m_particles[i];
//            ParticlePtr p2 = m_particles[j];
//            if (testParticleParticle(p1, p2)) {
//                ParticleParticleCollisionPtr c = std::make_shared<ParticleParticleCollision>(p1, p2, m_restitution);
//                m_collisions.push_back(c);
//            }
//        }
//    }
}

void DynamicSystem::solveCollisions() {
    while (!m_collisions.empty()) {
        CollisionPtr collision = m_collisions.back();
        collision->solveCollision();
        m_collisions.pop_back();
    }
}

void DynamicSystem::computeSimulationStep() {
    
    assert(! m_karts.empty());
    
    for(CubePtr c : m_karts){
        c->setForce(glm::vec3(0.0,0.0,0.0));
    }
    
    for(ForceFieldPtr f : m_forceFields){
        f->addForce();
    }
    
    //Integrate position and velocity of particles
    m_solver->solve(m_dt, m_karts);

    //Detect and resolve collisions
    if (m_handleCollisions) {
        detectCollisions();
        solveCollisions();
    }
}

const float DynamicSystem::getRestitution() {
    return m_restitution;
}

void DynamicSystem::setRestitution(const float& restitution) {
    m_restitution = std::max(0.0f, std::min(restitution, 1.0f));
}

std::ostream& operator<<(std::ostream& os, const DynamicSystemPtr& system) {
    std::vector<ParticlePtr> particles = system->getParticles();
    os << "Particle number: " << particles.size() << std::endl;
    for (ParticlePtr p : particles)
        os << p << std::endl;
    return os;
}
