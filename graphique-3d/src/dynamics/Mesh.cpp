#include "./../../include/dynamics/Mesh.hpp"
#include "../../include/log.hpp"

Mesh::Mesh(const std::vector<glm::vec3> &position,
           const std::vector<glm::vec3>& normals,
           const std::vector< unsigned int > indices,
           const glm::mat4 model) {
    m_position = position;
    m_indices = indices;
    m_normals = normals;
    m_model = model;
    init();
}

Mesh::~Mesh() {
}

void Mesh::init(){
    x_min = m_position[0].x;
    float x_max = m_position[0].x;
    y_min = m_position[0].y;
    float y_max = m_position[0].y;
    
    for(int i = 0; i<m_position.size(); i++) {
        if(m_position[i].x < x_min) x_min = m_position[i].x;
        if(m_position[i].x > x_max) x_max = m_position[i].x;
        if(m_position[i].y < y_min) y_min = m_position[i].y;
        if(m_position[i].y > y_max) y_max = m_position[i].y;
    }
    y_min++;
    y_max++;
    X = (int)(x_max-x_min)+1;
    Y = (int)(y_max-y_min)+1;
    
    m_sup = new std::vector<unsigned int>** [X];
    for (int i = 0; i < X; i++) {
        m_sup[i] = new std::vector<unsigned int>*[Y];
        for (int j = 0; j < Y; j++) {
            m_sup[i][j] = new std::vector<unsigned int>;
        }
    }
    
    for(int i = 0; i<m_indices.size()/3; i++){
        float xmin = m_position[m_indices[i*3]].x;
        float ymin = m_position[m_indices[i*3]].y;
        float xmax = xmin;
        float ymax = ymin;
        for(int j = 0; j<3; j++){
            if(xmin>m_position[m_indices[i*3+j]].x)xmin = m_position[m_indices[i*3+j]].x;
            if(xmax<m_position[m_indices[i*3+j]].x)xmax = m_position[m_indices[i*3+j]].x;
            if(ymin>m_position[m_indices[i*3+j]].x)ymin = m_position[m_indices[i*3+j]].y;
            if(ymax<m_position[m_indices[i*3+j]].x)ymax = m_position[m_indices[i*3+j]].y;
        }
        xmin -=x_min;
        xmax -=x_min;
        ymax -=y_min;
        ymin -=y_min;
       
        for(int Xcour = (int)(xmin+0.5); Xcour<=(int)(xmax+0.5); Xcour++){
            
            for(int Ycour = (int)(ymin+0.5); Ycour<=(int)(ymax+0.5); Ycour++){
                m_sup[Xcour][Ycour]->push_back(i*3);
            }
        }
        
    }

        
}
const std::vector<glm::vec3> & Mesh::getPosition() const {
    return m_position;
}

 const std::vector<glm::vec3>& Mesh::getNormals() const {
     return m_normals;
 }
   
 const std::vector<unsigned int>& Mesh::getIndices(float x, float y) const{
     int X_val =(int)(x-x_min+0.5);
     int Y_val =(int)(y-y_min+0.5);
     if(X_val>=0 && X_val<=X && Y_val>=0 && Y_val<=Y )
        return *m_sup[X_val][Y_val];
     std::vector<unsigned int> out;
     return out;
 }
 
 const std::vector<unsigned int>& Mesh::getIndices() const{
     return m_indices;
 }

const glm::mat4& Mesh::getModel() const {
    return m_model;
}

void Mesh::setModel(const glm::mat4 &model) {
    m_model = model;
}