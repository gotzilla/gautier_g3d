#include "./../../include/dynamics/MeshesCollision.hpp"
#include "./../../include/log.hpp"
#include <glm/gtx/norm.hpp>
#define SMALL_NUM   0.00000001


float distance = 0.0;
float reca = 0.1;
float dt = 0.01;

MeshesCollision::~MeshesCollision() {

}

MeshesCollision::MeshesCollision(MeshPtr mesh, CubePtr cube, float restitution, std::vector<int> indice, glm::vec3 I) :
Collision(restitution) {
    m_m = mesh;
    m_c = cube;
    m_indice = indice;
    pt_inter = I;
}
//
////retourne les coordonnées de P2 a changer
//glm::vec3 InterSphereDroite(glm::vec3 P1, glm::vec3 P2, glm::vec3 D1, glm::vec3 dir){
//    LOG(debug, "coordonnes P1 :"<<P1.x<<", "<<P1.y<<", "<<P1.z);
//    LOG(debug, "coordonnes P2 :"<<P2.x<<", "<<P2.y<<", "<<P2.z);
//    LOG(debug, "coordonnes D1 :"<<D1.x<<", "<<D1.y<<", "<<D1.z);
//    LOG(debug, "coordonnes dir :"<<dir.x<<", "<<dir.y<<", "<<dir.z);
//    float R2 = glm::dot(P2-P1, P2-P1);
//    float a = glm::dot(dir, dir);
//    float b = 2*(glm::dot(D1-P1, dir));
//    float c = glm::dot(D1, D1) + glm::dot(P1, P1) - 2 * glm::dot(P1, D1) - R2;
//    float delta;
//    if(delta = b*b-4*a*c > 0) {//forcement vrai
//        float s1 = (-b + 2*sqrt(delta))/(2*a); 
//        float s2 = (-b - 2*sqrt(delta))/(2*a); 
//        if(abs(s1)<abs(s2)) //on prend la plus petite distance
//            return D1+s1*dir;
//        
//        return D1+s2*dir;
//    }
//    LOG(debug, "resolution impossible");
//    return P2; //vecteur null
//}
//
////prérequis avoir les deux points du meme coté du shape
//glm::vec3 InterDroitePlan(glm::vec3 D1, glm::vec3 D2, glm::vec3 P, glm::vec3 n) {
//    
////distance des points au plan    
//    float d1 = glm::dot(P - D1, n);
//    float d2 = glm::dot(P - D2, n);
//
//    //points projetés sur le plan
//    glm::vec3 D1P = D1 + d1 * n;
//    glm::vec3 D2P = D2 + d2 * n;
//    
//    return D1P + (D2P-D1P)*d2/d1;
//}
////
////N_drt est le vecteur directeur de l'autre coté adjacent du rectangle
//glm::vec3 NouvellePosition(glm::vec3 P1_drt, glm::vec3 P2_drt, glm::vec3 N_drt, glm::vec3 N_Plan, glm::vec3 P_plan) {
////
////   glm::vec3 u = glm::normalize(glm::cross(glm::cross(P2_drt-P1_drt, N_Plan), N_Plan));
////    
////   glm::vec3 I = glm::dot(P2_drt-P_plan, u)*u + P_plan;
////   LOG(debug, "coordonnes I :"<<I.x<<", "<<I.y<<", "<<I.z);
////   LOG(debug, "coordonnes P1 :"<<P1_drt.x<<", "<<P1_drt.y<<", "<<P1_drt.z);
////   LOG(debug, "coordonnes P2 :"<<P2_drt.x<<", "<<P2_drt.y<<", "<<P2_drt.z);
////   LOG(debug, "distance de PPlan a P2 :"<<glm::dot(P2_drt-P_plan, u));
////    float d_carre = sqrt(glm::dot(P2_drt-P1_drt, P2_drt-P1_drt));
////    LOG(debug, "dist : "<<d_carre);
////    glm::vec3 P = I;
////    float dist = sqrt(glm::dot(P-P1_drt, P-P1_drt)) - d_carre;
////    LOG(debug, "distance : "<<dist);
////    bool continuer = dist<-0.01 || dist>0.01;
////    while(continuer) {
////        P -=  dist * u;
////        dist = glm::dot(P-P1_drt, P-P1_drt) - d_carre;
////        continuer = dist<-0.01 || dist>0.01;
////        }
////    return P;
//
//
////
//    glm::vec3 IntDrtPln = InterDroitePlan(P1_drt, (float)2*P1_drt-P2_drt, P_plan, N_Plan);
//    glm::vec3 dirDrtInter = glm::normalize(glm::cross(N_drt, N_Plan));
//    
//    return InterSphereDroite(P1_drt, P2_drt, IntDrtPln, dirDrtInter);
//
//
//
//    
//}


void calculPosition(glm::vec3 fixe, glm::vec3 Bouge, glm::vec3 BougeInit, glm::vec3 & centre, glm::vec3 & normale, glm::vec3 & dir, glm::vec3 & vitesse, glm::vec3 vitesse2) {
    
    glm::vec3 axe = glm::normalize(glm::cross(BougeInit - fixe, Bouge - BougeInit));
   
    
    glm::vec3 normale_bis = glm::normalize(glm::cross(axe, Bouge - fixe));
        
    centre = glm::dot(centre - fixe, normale)*normale_bis + glm::dot(centre-fixe, glm::normalize(BougeInit - fixe))*glm::normalize(Bouge-fixe) + fixe;
    
    dir = glm::dot(dir, axe)*axe + glm::dot(dir, glm::normalize(BougeInit - fixe))*glm::normalize(Bouge-fixe); 
    
    vitesse += glm::dot(vitesse2, glm::normalize(BougeInit-fixe))*glm::normalize(Bouge-fixe)+glm::dot(vitesse2, axe)*axe+glm::dot(vitesse2, normale)*normale_bis;
    
    
    normale = normale_bis;
        
}


glm::vec3 calculVRebondRelatif(glm::vec3 Varrive, glm::vec3 Normale) {
    float k = 1.5;
    
    return  - k * glm::dot(Varrive, Normale)*Normale;
}


//
//
////maj de la vitesse et il te le mat4x4 correspondant a la transformation
//void TransformationTotale(glm::vec3 &centre, glm::vec3 &normale, glm::vec3 &direction, glm::vec3 PointCubeFixe,
//                                 glm::vec3 PointCubeContact, glm::vec3 PointCubeAutre, glm::vec3 NPlan, glm::vec3 PPlan,
//                                 glm::vec3 & vitesse) {
//    glm::vec3 PointTransf = NouvellePosition(PointCubeAutre, PointCubeContact, glm::normalize(PointCubeAutre-PointCubeFixe), NPlan, PPlan);
//    glm::vec3 centrep = centre;
//    glm::vec3 normalep = normale;
//    glm::vec3 directionp = direction;
//  
//    vitesse += calculVRebondRelatif(vitesse, NPlan);
//    calculPosition(PointCubeFixe, PointTransf, PointCubeContact, centre, normale, direction, vitesse);
//  
//   
//}
void Transfo(glm::vec3 &centre, glm::vec3 &normale, glm::vec3 &dir, glm::vec3 P1, glm::vec3 PI, glm::vec3 P2, glm::vec3 &V, glm::vec3 NP){
    
    glm::vec3 depl = P1-P2;
    float reste = (float)(glm::distance(PI,P2)/glm::distance(P1, P2));
    V += calculVRebondRelatif(V, NP);
    
        glm::vec3 suiv = reste * V;
    glm::vec3 fixe = (float)2 * centre - P2 + (float)2*glm::dot(P2-centre,normale)*normale;
    glm::vec3 dir1 = glm::normalize(PI-fixe);
    glm::vec3 dir2 = glm::normalize(P2-fixe);
    glm::vec3 axe = glm::normalize(glm::cross(dir1, dir2));
    glm::vec3 ort1 = glm::normalize(glm::cross(axe, dir1));
    glm::vec3 ort2 = glm::normalize(glm::cross(axe, dir2));
    centre = glm::dot(centre-fixe, axe)*axe +glm::dot(centre-fixe, dir1)*dir2 +glm::dot(centre-fixe, ort1)*ort2 +fixe;
    dir = glm::dot(dir, axe)*axe +glm::dot(dir, dir1)*dir2 +glm::dot(dir, ort1)*ort2;
    normale = glm::dot(normale, axe)*axe +glm::dot(normale, dir1)*dir2 +glm::dot(normale, ort1)*ort2;
    
//    glm::vec3 liais_centre = centre - P1;
//    glm::vec3 centre_bis = centre + (float)2*liais_centre - glm::dot(liais_centre, NP)*NP;
//    glm::vec3 axe = glm::normalize(glm::cross(centre - P1, centre_bis-P1));
//    glm::vec3 cen_ort = glm::normalize(glm::cross(axe, centre-P1));
//    glm::vec3 cen_b_ort = glm::normalize(glm::cross(axe, centre_bis-P1));
//    glm::vec3 axe_cent = glm::normalize(centre - P1);
//    glm::vec3 axe_cent_b = glm::normalize(centre_bis - P1);
//    normale = glm::dot(normale, cen_ort)*cen_b_ort + glm::dot(normale, axe)*axe + glm::dot(normale, axe_cent)*axe_cent_b;
//    dir = glm::dot(dir, cen_ort)*cen_b_ort + glm::dot(dir, axe)*axe + glm::dot(dir, axe_cent)*axe_cent_b;
//    centre = centre + depl + V*(float)0.01*reste;
//    
//    
    
//    
//    
//    glm::vec3 fixe = (float)2 * centre - P2 + (float)2*glm::dot(P2-centre,normale)*normale;
//    glm::vec3 v2 = V + calculVRebondRelatif(V, NP);
//    float temps = (float)(1 - (double)glm::distance(P1, PI)/(double)glm::distance(P1, P2));
//    calculPosition(fixe, P2 + v2*temps, P2, centre, normale, dir, V, v2);
//    centre += PI-P2+V*temps*(float)0.01;   
}
void MeshesCollision::do_solveCollision() {
    //Compute interpenetration distance
//    
//    
    std::vector< unsigned int > indices = m_m->getIndices();
    
    //en premier indice on a l'indice du rectangle et en deuxieme celui du mesh
    
    
        glm::vec3 pt_1 = m_c->getPositions()[m_indice[0]];
        glm::vec3 pt_2 = pt_1 - dt*m_c->getVelocity();
        
        glm::vec3 PPlan = m_m->getPosition()[indices[m_indice[1]]];
        glm::vec3 PPlan2 = m_m->getPosition()[indices[m_indice[1]+1]];
        glm::vec3 PPlan3 = m_m->getPosition()[indices[m_indice[1]+2]];
        glm::vec3 NPlan = glm::normalize(glm::cross(PPlan - PPlan2, PPlan3-PPlan2));
        glm::vec3 centre = m_c->getPosition();
        glm::vec3 normale = m_c->getNormale();
        glm::vec3 dir = m_c->getDirection();
        glm::vec3 vitesse = m_c->getVelocity();
        float deux = 2;
//        if(glm::dot(NPlan, normale)<0) NPlan = - NPlan;
//        glm::vec3 axe = glm::normalize(glm::cross(normale, NPlan));
//        dir = glm::normalize(glm::cross(axe, NPlan));
//        normale = NPlan;            
//        vitesse += calculVRebondRelatif(vitesse, NPlan);
//        centre += NPlan;
        
        if(glm::dot(NPlan, normale)<0) NPlan = -NPlan;
        Transfo(centre, normale, dir, pt_1, pt_inter, pt_2, vitesse, NPlan);
        centre += normale*reca;
        m_c->actualise(centre, normale, dir, glm::vec3(0,0,0));//vitesse);
        m_indice.pop_back();
        m_indice.pop_back();
        m_indice.clear();
    

}


//http://geomalgorithms.com/a06-_intersect-2.html
// intersect3D_RayTriangle(): find the 3D intersection of a ray with a triangle
//    Input:  a ray R, and a triangle T
//    Output: *I = intersection point (when it exists)
//    Return: -1 = triangle is degenerate (a segment or point)
//             0 =  disjoint (no intersect)
//             1 =  intersect in unique point I1
//             2 =  are in the same plane
int intersect3D_RayTriangle(glm::vec3 R1, glm::vec3 R2, glm::vec3 T1, glm::vec3 T2, glm::vec3 T3, glm::vec3 * I) {
    glm::vec3 u, v, n; // triangle vectors
    glm::vec3 dir, w0, w; // ray vectors
    float r, a, b; // params to calc ray-plane intersect

    // get triangle edge vectors and plane normal
    u = T1 - T2;
    v = T3 - T2;
    n = glm::cross(u , v); // cross product
    if (n.x == 0 && n.y == 0 && n.z ==0) { // triangle is degenerate
        return -1; // do not deal with this case
    }
    n = glm::normalize(n);
    glm::vec3 secu = distance * n;
    dir = R2 - R1; // ray direction vector
    w0 = R1 - T1-secu;
    a = -glm::dot(n, w0);
    b = glm::dot(n, dir);
    if (fabs(b) < SMALL_NUM) { // ray is  parallel to triangle plane
        if (a == 0) // ray lies in triangle plane
                    //le cote appartient au triangle
            return 2;
        else return 0; // ray disjoint from plane
    }

    // get intersect point of ray with triangle plane
    r = a / b;
    if(r<0.0){// ray goes away from triangle
        return 0; // => no intersect
    }
    // for a segment, also test if (r > 1.0) => no intersect

    *I = R1 + r * dir; // intersect point of ray and plane

    // is I inside T?
    float uu, uv, vv, wu, wv, D;
    uu = glm::dot(u, u);
    uv = glm::dot(u, v);
    vv = glm::dot(v, v);
    w = *I - T1-secu;
    wu = glm::dot(w, u);
    wv = glm::dot(w, v);
    D = uv * uv - uu * vv;

    // get and test parametric coords
    float s, t;
    s = (uv * wv - vv * wu) / D;
    if (s < 0.0 || s > 1.0) // I is outside T
        return 0;
    t = (uv * wu - uu * wv) / D;
    if (t < 0.0 || (s + t) > 1.0) // I is outside T
        return 0;
    return 1; // I is in T
}

int calcul(glm::vec3 P, glm::vec3 V, glm::vec3 N) {
    float out = 0;
    for(int i = 0; i<3; i++) {
        out += P[i%3]*(V[(i+1)%3] * N[(i+2)%3] -N[(i+1)%3] * V[(i+2)%3]);
        //LOG(debug, "valeur coord "<<i<<" : "<<P[i%3]*(V[(i+1)%3] * N[(i+2)%3] -N[(i+1)%3] * V[(i+2)%3])<<" , out : "<<out);
    }
    if(out>0) {
      //  LOG(debug, "positif");
        return 1;
    }
    //LOG(debug, "négatif");
    return -1;
}


bool inside(glm::vec3 P, glm::vec3 P1, glm::vec3 P2, glm::vec3 P3, glm::vec3 n) {

    int i = 0;
    
    i += calcul(P-P1, P2-P1, n);
    i += calcul(P-P2, P3-P2, n);
    i += calcul(P-P3, P1-P3, n);
    
    if(abs(i)==3) {
        //LOG(debug, "true");
        return true;
    }
    return false;
    
}




bool testKartMap(const MeshPtr &m, const CubePtr &c, std::vector<int> & indice, glm::vec3 &out) {

    
    glm::vec3 centre = c->getPosition();
    glm::vec3 V = c->getVelocity()*dt;
    
    std::vector< unsigned int > indices;
            std::vector< unsigned int > indices2 = m->getIndices(centre.x, centre.y);
            for(int p : indices2){
                indices.push_back(p);
            }
    float incrx = 1;
    float incry = 1;
    if(V.x>0) incrx = -1;
    if(V.y>0) incry = -1;
    for(float x = -1; x<=1; x ++){
        for(float y = -1; y<=1; y ++){
            indices2 = m->getIndices(x+centre.x, y+centre.y);
            for(int p : indices2){
                indices.push_back(p);
            }
        }
        
    }
    
    
    //LOG(debug, "nb facettes a l'applond : "<< indices.size());
    std::vector< unsigned int > mindices = m->getIndices();

    std::vector< glm::vec3 > vec = m->getPosition();
    std::vector<glm::vec3> cube;

    glm::mat4 modelMap = m->getModel();
    glm::mat4 modelCube = c->getModel();

    for (glm::vec3 v : c->getPositions()) {
        glm::vec4 c = glm::inverse(modelMap) * modelCube * glm::vec4(v, 0);
        cube.push_back(glm::vec3(c[0], c[1], c[2]));
    }

    //4 coins
    std::vector<int> cbe;
    cbe.push_back(0);
    cbe.push_back(13);
    cbe.push_back(12);
    cbe.push_back(4);
        
    cbe.push_back(7);
    cbe.push_back(1);
    cbe.push_back(8);
    cbe.push_back(2);

    
    glm::vec3 Res1;
    glm::vec3 Res2;
    float res1=1;
    float res2=1;
    int mem_c = -1;
    int mem_m = -1;
    int nb_inter = 0;


        //Triangle to test
    //LOG(debug, "size"<<indices.size());
    //LOG(debug, "vec size"<<vec.size());
    for(int i = 0; i<indices.size(); i++) {
        glm::vec3 P1 = vec[mindices[indices[i]]];
        glm::vec3 P2 = vec[mindices[indices[i]+1]];
        glm::vec3 P3 = vec[mindices[indices[i]+2]];
        for( int Tmp : cbe) {
            glm::vec3 tmp = cube[Tmp];
            int resultat = intersect3D_RayTriangle(tmp, tmp-V, P1, P2, P3, &Res1);
            //LOG(debug, "result"<<resultat);
            if(resultat==1) {
                res1 = glm::distance(tmp, Res1)/glm::distance(tmp, tmp-V);
                //LOG(debug, "distance : "<<res1);
                if (res1 < 1) {
                //LOG(debug, "distance : "<<res1);
                
                    res2 = res1;
                    Res2 = Res1;
                    mem_c = Tmp;
                    mem_m = indices[i];
                    nb_inter++;
                }
            }
        }
    }
    c->au_sol(false);
    if(nb_inter>=2){
        glm::vec3 n = glm::normalize(glm::cross(vec[mem_m]-vec[mem_m+1], vec[mem_m+2]-vec[mem_m+1]));
        if(glm::dot(n, c->getNormale())<0) n = -n;
        indice.push_back(mem_c);
        indice.push_back(mem_m);
        out = Res2;  
        c->au_sol(true);
        return true;
        
        
    }
    if (nb_inter==1) {
        indice.push_back(mem_c);
        indice.push_back(mem_m);
        out = Res2;  
        c->au_sol(true);
        return true;
    }
    
    return false;
}

//
//
//bool testKartMap(const MeshPtr &m, const CubePtr &c, std::vector<int> & out) {
//    
//    std::vector< glm::vec3 > vec = m->getPosition();
//    std::vector< unsigned int > indices = m->getIndices();
//    bool boul = false;
//    glm::vec3 P;
//  //  LOG(debug, "debut detection");
//    for(int i = 0; i<indices.size()/3; i++) {
//        glm::vec3 P1 = vec[indices[i*3]];
//        glm::vec3 P2 = vec[indices[i*3]+1];
//        glm::vec3 P3 = vec[indices[i*3]+2];
//        glm::vec3 n;
//        float scale = 0;
//        n = glm::normalize(glm::cross(P1-P2, P3-P2));
//        
//        //calcul des points du kart
//        
//        for(int j = 0; j<7; j++) {
//            P = c->getPositions()[j];
//            
//            float dist = glm::dot(P - P1, n);
//            if(dist < distance && dist > 0) {
//                // LOG(debug, "distance "<<dist);
//                glm::vec3 PP = P - dist*n;
//                //projection d'un point du triangle sur le segment opposé, puis on construit un vecteur avec le seommet et son projeté!
//                if(inside(PP, P1, P2, P3, n)) {
//
//                    out.push_back(j);
//                    out.push_back(i*3);
//                    boul=true;
//                    //LOG(debug, "collision");
//                     //collision
//
//                }
//            }
//        }
//    }
//    return boul;
//}