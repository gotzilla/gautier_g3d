#include <vector>

#include "./../../include/dynamics/Cube.hpp"
#include "./../../include/Utils.hpp"
#include "../../include/log.hpp"
#include "../../include/Io.hpp"

Cube::Cube( const glm::vec3& position,
            const glm::vec3& velocity,
            const glm::vec3& direction,
            const glm::vec3& normale,
            const float& mass,
            const glm::mat4 & model)
        :m_initialVelocity(velocity),
        m_initialPosition(position),
        m_initialDirection(direction),    
        m_initialNormale(normale)    
        {
    
    m_model= model;
    m_mass=mass;
    m_force =glm::vec3(0.0, 0.0, 0.0);
    
    std::vector< glm::vec3 > normals;
    std::vector<glm::vec2> tmp_tex;
    m_positions_init.clear();
    MaterialPtr m_material;
    std::string texture = "";
    int index;
    std::vector<unsigned int> tmp;
    //create_obj( m_positions_init, tmp,  normals, tmp_tex, m_material, texture, index, "../meshes/obj/kart2.obj");
    getUnitCube(m_positions_init, normals, tmp_tex);
    
    m_position = position;
    m_position_prec = position;
    m_normale = glm::normalize(normale);
    m_direction = glm::normalize(direction);
    m_velocity = velocity;
    m_axe = glm::normalize(glm::cross(normale, direction));    
    m_positions.clear();
    actualise(position, normale, direction, velocity);
}

Cube::~Cube() {
}

bool Cube::au_sol() const{
    return m_au_sol;            
}

void Cube::au_sol(bool in){
    m_au_sol = in;            
}

void Cube::turn_right(){
   m_direction = glm::normalize(m_direction + (float)0.01*m_axe);
   m_axe = glm::normalize(glm::cross(m_normale, m_direction));
   m_modif = true;
   actualise();
}

void Cube::turn_left(){
    m_direction = glm::normalize(m_direction - (float)0.01*m_axe);
    m_axe = glm::normalize(glm::cross(m_normale, m_direction));
    m_modif = true;
    actualise();
}

void Cube::accelerate(){
   // if(!au_sol()) {
//        m_direction = glm::normalize(m_direction - (float)0.005*m_normale);
//        m_normale = glm::normalize(glm::cross(m_axe, m_normale));
    //} else {
        m_velocity += (float)0.5*m_direction;
    //}
    m_modif = true;
    actualise();
}

void Cube::decelerate(){
    //if(!au_sol()) {
//        m_direction = glm::normalize(m_direction + (float)0.005*m_normale);
//        m_normale = glm::normalize(glm::cross(m_axe, m_normale));
    //} else {
        m_velocity -= (float)0.5*m_direction;
    //}
    m_modif = true;
    actualise();
}

bool Cube::modif() const {
    return m_modif;
}

void Cube::setModel(const glm::mat4 &model) {
    m_model = model;
}

const glm::mat4& Cube::getModel() const {
    return m_model;
}

const glm::vec3& Cube::getPositionPrec() const {
    return m_position_prec;
}

const glm::vec3& Cube::getPosition() const {
    return m_position;
}

const std::vector<glm::vec3>& Cube::getPositions() const {
    return m_positions;
}

const glm::vec3& Cube::getDirection() const {
    return m_direction;
}

const glm::vec3& Cube::getNormale() const {
    return m_normale;
}

const glm::vec3 & Cube::getVelocity() const {
    return m_velocity;
}

const glm::vec3 & Cube::getForce() const {
    return m_force;
}

float Cube::getMass() const {
    return m_mass;
}

std::vector< glm::vec3 > Cube::getForces() const {
    return m_forces;
}


void Cube::actualise(){
    for(glm::vec3 p : m_positions_init) {
        m_positions.push_back((p[1]*m_axe + p[0]*m_direction + p[2]*m_normale)*(float)0.1 + m_position);
    }
    m_modif = false;
}
void Cube::actualise(glm::vec3 position, glm::vec3 normale, glm::vec3 dir, glm::vec3 vitesse) {
    m_position_prec = m_position;
    m_position = position;
    m_normale = glm::normalize(normale);
    m_direction = glm::normalize(dir);
    m_velocity = vitesse;
    m_axe = glm::normalize(glm::cross(normale, dir));    
    m_positions.clear();
    glm::vec3 out;
    actualise();
}



void Cube::setPosition(const glm::vec3 &pos) {
    m_position_prec = m_position;
    glm::vec3 depl = pos - m_position;
    m_position = pos;
    std::vector<glm::vec3> actual;
    for(glm::vec3 p : m_positions) {
        actual.push_back(p + depl);
    }
    m_positions = actual;
}

void Cube::setVelocity(const glm::vec3 &vitesse) {
    
    float K = 0.5;
    float k = 0.1;
    m_velocity = -glm::dot(vitesse, m_direction)*m_direction*k;
    m_velocity += vitesse - glm::dot(vitesse, m_direction)*m_direction*K;
}

void Cube::addForce(const glm::vec3 &force) {
    m_forces.push_back(force);
}

void Cube::setForce(const glm::vec3 &force) {
    m_force = force;
}

void Cube::incrForce(const glm::vec3& force) {
    m_force += force;
}

void Cube::restart() {
    LOG(debug, "restart");
    actualise(m_initialPosition, m_initialNormale, m_initialDirection, m_initialVelocity);
}
