#include "./../../include/dynamics/SolidSolver.hpp"
#include "./../../include/log.hpp"

SolidSolver::SolidSolver() {

}

SolidSolver::~SolidSolver() {

}

void SolidSolver::do_solve(const float& dt, std::vector<CubePtr>& karts) {
    for (CubePtr k : karts) {
        //translation
        
        if(k->modif()) k->actualise();
        k->setVelocity(k->getVelocity() + dt * (1.0f / k->getMass()) * k->getForce());

        k->setPosition(k->getPosition()+ dt * k->getVelocity());
        //rotation
        //        for (glm::vec3 v : k->getPosition()){
        //            LOG(debug, "" << v.x << " / " << v.y << " / " << v.z);
        //        }
        //        float a = k->getPosition()[2].x - k->getPosition()[3].x;
        //        float b = k->getPosition()[4].y - k->getPosition()[3].y;
        //        float c = k->getPosition()[13].z - k->getPosition()[3].z;
        //        float Ix = k->getMass() / 12 * (b * b + c * c);
        //        float Iy = k->getMass() / 12 * (a * a + c * c);
        //        float Iz = k->getMass() / 12 * (a * a + b * b);
        //        glm::vec3 I = glm::vec3(1 / Ix, 1 / Iy, 1 / Iz);
        //        glm::vec3 sM = glm::vec3((float) 0, (float) 0, (float) 0);
        //
        //        int i = 0;
        //        for (glm::vec3 f : k->getForces()) {
        //            sM += glm::cross(k->getPForces()[i] - k->getPosition()[0], f);
        //
        //            i++;
        //        }
        //        k->setVAngle(glm::dot(I, sM) * dt + k->getVAngle());
        //        k->setAngle(k->getAngle() + dt * k->getVAngle());
        //
        //        float co;
        //        float s;
        //        co = cos(k->getAngle().x);
        //        s = sin(k->getAngle().x);
        //        glm::mat3 Rx = glm::mat3(
        //                1,
        //                0,
        //                0,
        //                0,
        //                co,
        //                s,
        //                0,
        //                s,
        //                co);
        //        co = cos(k->getAngle().y);
        //        s = sin(k->getAngle().y);
        //        glm::mat3 Ry = glm::mat3(
        //                co,
        //                0,
        //                s,
        //                0,
        //                1,
        //                0,
        //                -s,
        //                0,
        //                co);
        //        co = cos(k->getAngle().z);
        //        s = sin(k->getAngle().z);
        //        glm::mat3 Rz = glm::mat3(
        //                co,
        //                -s,
        //                0,
        //                s,
        //                co,
        //                0,
        //                0,
        //                0,
        //                1);
        //        glm::vec3 deplacement;
        //        i = 0;
        //        for (glm::vec3 p : k->getPosition()) {
        //            deplacement = Rx * p;
        //            deplacement += Ry * p;
        //            deplacement += Rz * p;
        //            lP[i] = p[i] + deplacement;
        //        }
        //        k->setPosition(lP);

    }
}
