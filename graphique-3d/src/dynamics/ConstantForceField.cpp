#include "./../../include/dynamics/ConstantForceField.hpp"
#include "./../../include/log.hpp"

ConstantForceField::ConstantForceField(const std::vector<CubePtr>& karts, const glm::vec3& force) {
    m_karts = karts;
    m_force = force;
}

void ConstantForceField::do_addForce() {
    for (CubePtr k : m_karts) {
        k->incrForce(m_force * k->getMass());
        k->addForce(m_force * k->getMass());
    
    }
    
}

const std::vector<CubePtr> ConstantForceField::getKarts() {
    return m_karts;
}

void ConstantForceField::setKarts(const std::vector<CubePtr>& cubes) {
    m_karts = cubes;
}

const glm::vec3& ConstantForceField::getForce() {
    return m_force;
}

void ConstantForceField::setForce(const glm::vec3& force) {
    m_force = force;
}
