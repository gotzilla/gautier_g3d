#include "./../include/CubeRenderable.hpp"
#include "./../include/gl_helper.hpp"
#include "./../include/log.hpp"
#include "./../include/Utils.hpp"
#include "../include/Viewer.hpp"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <GL/glew.h>

CubeRenderable::CubeRenderable(ShaderProgramPtr shaderProgram, CubePtr kart)
: HierarchicalRenderable(shaderProgram),
m_pBuffer(0), m_cBuffer(0), m_nBuffer(0), m_cube(kart) {
    approximatedCenter = glm::vec3(0.5, 0.5, 0.5);

    std::vector<glm::vec2> tmp_tex;
    getUnitCube(m_positions, m_normals, tmp_tex);

    //Assign one color to each face
    m_colors.push_back(glm::vec4(1, 0, 0, 1));
    m_colors.push_back(glm::vec4(1, 0, 0, 1));
    m_colors.push_back(glm::vec4(1, 0, 0, 1));

    m_colors.push_back(glm::vec4(0, 1, 0, 1));
    m_colors.push_back(glm::vec4(0, 1, 0, 1));
    m_colors.push_back(glm::vec4(0, 1, 0, 1));

    m_colors.push_back(glm::vec4(0, 0, 1, 1));
    m_colors.push_back(glm::vec4(0, 0, 1, 1));
    m_colors.push_back(glm::vec4(0, 0, 1, 1));

    m_colors.push_back(glm::vec4(0, 1, 1, 1));
    m_colors.push_back(glm::vec4(0, 1, 1, 1));
    m_colors.push_back(glm::vec4(0, 1, 1, 1));

    m_colors.push_back(glm::vec4(1, 0, 1, 1));
    m_colors.push_back(glm::vec4(1, 0, 1, 1));
    m_colors.push_back(glm::vec4(1, 0, 1, 1));

    m_colors.push_back(glm::vec4(1, 1, 0, 1));
    m_colors.push_back(glm::vec4(1, 1, 0, 1));
    m_colors.push_back(glm::vec4(1, 1, 0, 1));

    m_colors.push_back(glm::vec4(0, 0.5, 0.5, 1));
    m_colors.push_back(glm::vec4(0, 0.5, 0.5, 1));
    m_colors.push_back(glm::vec4(0, 0.5, 0.5, 1));

    m_colors.push_back(glm::vec4(0.5, 0, 0.5, 1));
    m_colors.push_back(glm::vec4(0.5, 0, 0.5, 1));
    m_colors.push_back(glm::vec4(0.5, 0, 0.5, 1));

    m_colors.push_back(glm::vec4(0.5, 0.5, 0, 1));
    m_colors.push_back(glm::vec4(0.5, 0.5, 0, 1));
    m_colors.push_back(glm::vec4(0.5, 0.5, 0, 1));

    m_colors.push_back(glm::vec4(0.8, 0.2, 0, 1));
    m_colors.push_back(glm::vec4(0.8, 0.2, 0, 1));
    m_colors.push_back(glm::vec4(0.8, 0.2, 0, 1));

    m_colors.push_back(glm::vec4(0.2, 0, 0.8, 1));
    m_colors.push_back(glm::vec4(0.2, 0, 0.8, 1));
    m_colors.push_back(glm::vec4(0.2, 0, 0.8, 1));

    m_colors.push_back(glm::vec4(0, 0.8, 0.2, 1));
    m_colors.push_back(glm::vec4(0, 0.8, 0.2, 1));
    m_colors.push_back(glm::vec4(0, 0.8, 0.2, 1));

    //Create buffers
    glGenBuffers(1, &m_pBuffer); //vertices
    glGenBuffers(1, &m_cBuffer); //colors
    glGenBuffers(1, &m_nBuffer); //normals

    //Activate buffer and send data to the graphics card
    glcheck(glBindBuffer(GL_ARRAY_BUFFER, m_pBuffer));
    glcheck(glBufferData(GL_ARRAY_BUFFER, m_positions.size() * sizeof (glm::vec3), m_positions.data(), GL_STATIC_DRAW));
    glcheck(glBindBuffer(GL_ARRAY_BUFFER, m_cBuffer));
    glcheck(glBufferData(GL_ARRAY_BUFFER, m_colors.size() * sizeof (glm::vec4), m_colors.data(), GL_STATIC_DRAW));
    glcheck(glBindBuffer(GL_ARRAY_BUFFER, m_nBuffer));
    glcheck(glBufferData(GL_ARRAY_BUFFER, m_normals.size() * sizeof (glm::vec3), m_normals.data(), GL_STATIC_DRAW));
}

std::vector< glm::vec3 >& CubeRenderable::getPositions(){
    return m_positions;
}

void CubeRenderable::do_draw() {
    //Update the parent and local transform matrix to position the geometric data according to the particle's data.
    m_positions.clear();

    for (glm::vec3 v : m_cube->getPositions()){
        m_positions.push_back(v);
        
    //    setLocalTransform(glm::translate(glm::mat4(1.0), v));
    }
    glcheck(glBindBuffer(GL_ARRAY_BUFFER, m_pBuffer));
    glcheck(glBufferData(GL_ARRAY_BUFFER, m_positions.size() * sizeof (glm::vec3), m_positions.data(), GL_STATIC_DRAW));
    //Location
    int positionLocation = m_shaderProgram->getAttributeLocation("vPosition");
    int colorLocation = m_shaderProgram->getAttributeLocation("vColor");
    int normalLocation = m_shaderProgram->getAttributeLocation("vNormal");
    int modelLocation = m_shaderProgram->getUniformLocation("modelMat");

    //Send data to GPU
    if (modelLocation != ShaderProgram::null_location) {
        glcheck(glUniformMatrix4fv(modelLocation, 1, GL_FALSE, glm::value_ptr(getModelMatrix())));
    }

    if (positionLocation != ShaderProgram::null_location) {
        //Activate location
        glcheck(glEnableVertexAttribArray(positionLocation));
        //Bind buffer
        glcheck(glBindBuffer(GL_ARRAY_BUFFER, m_pBuffer));
        //Specify internal format
        glcheck(glVertexAttribPointer(positionLocation, 3, GL_FLOAT, GL_FALSE, 0, (void*) 0));
    }

    if (colorLocation != ShaderProgram::null_location) {
        glcheck(glEnableVertexAttribArray(colorLocation));
        glcheck(glBindBuffer(GL_ARRAY_BUFFER, m_cBuffer));
        glcheck(glVertexAttribPointer(colorLocation, 4, GL_FLOAT, GL_FALSE, 0, (void*) 0));
    }

    if (normalLocation != ShaderProgram::null_location) {
        glcheck(glEnableVertexAttribArray(normalLocation));
        glcheck(glBindBuffer(GL_ARRAY_BUFFER, m_nBuffer));
        glcheck(glVertexAttribPointer(normalLocation, 3, GL_FLOAT, GL_FALSE, 0, (void*) 0));
    }

    //Draw triangles elements
    glcheck(glDrawArrays(GL_TRIANGLES, 0, m_positions.size()));

    if (positionLocation != ShaderProgram::null_location) {
        glcheck(glDisableVertexAttribArray(positionLocation));
    }
    if (colorLocation != ShaderProgram::null_location) {
        glcheck(glDisableVertexAttribArray(colorLocation));
    }
    if (normalLocation != ShaderProgram::null_location) {
        glcheck(glDisableVertexAttribArray(normalLocation));
    }
}


CubeRenderable::~CubeRenderable() {
    glcheck(glDeleteBuffers(1, &m_pBuffer));
    glcheck(glDeleteBuffers(1, &m_cBuffer));
    glcheck(glDeleteBuffers(1, &m_nBuffer));
}


void CubeRenderable::do_keyPressedEvent( sf::Event& e )
{
    if( e.key.code == sf::Keyboard::Left )
    {
        m_cube->m_left=true;
    }
    else if( e.key.code == sf::Keyboard::Right )
    {
        m_cube->m_right = true;
    }
    else if( e.key.code == sf::Keyboard::Up )
    {
        m_cube->m_accelerate = true;
    }
    else if( e.key.code == sf::Keyboard::Down )
    {
        m_cube->m_decelerate = true;
    }
}

void CubeRenderable::do_keyReleasedEvent( sf::Event& e )
{
    if( e.key.code == sf::Keyboard::Left )
    {
        m_cube->m_left=false;
    }
    else if( e.key.code == sf::Keyboard::Right )
    {
        m_cube->m_right = false;
    }
    else if( e.key.code == sf::Keyboard::Up )
    {
        m_cube->m_accelerate = false;
    }
    else if( e.key.code == sf::Keyboard::Down )
    {
        m_cube->m_decelerate = false;
    }
}

void CubeRenderable::do_animate( float time )
{
    if( time > m_cube->m_last_time )
    {
        float dt = time - m_cube->m_last_time;

        if ( m_cube->m_left && !m_cube->m_right )
        {
            m_cube->turn_right();
        }
        else if( m_cube->m_right && !m_cube->m_left )
        {
            m_cube->turn_left();
        }

        if( m_cube->m_accelerate )
            m_cube->accelerate();
        else if( m_cube->m_decelerate )
            m_cube->decelerate();
        
    }
    m_cube->m_last_time = time;
}
